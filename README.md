# GitSharpCms
#### Git based Content Management System for .NET

GitSharpCms is a **super-lightweight CMS** implementation based on [git's content management capabilities](https://git-scm.com/book/en/v1/Getting-Started-Git-Basics) on the .NET platform.
Technically it's a **.NET Standard 2.0 library** that can be used in any full .NET or .NET Core project. It is basically a wrapper around [LibGit2Sharp](https://github.com/libgit2/libgit2sharp) (which is itself a .NET wrapper around [libgit2](https://libgit2.org/)).

#### Does git have to be installed to use GitSharpCmd?
The short answer is: no, but that can be useful.
The long answer is: the git executable is a coherent collection of functions that can deal with git repositories (files, branches, network communication, etc) exposed via command line interface.
LibGit2(Sharp) impements the same set of functions and exposes them via an API. So you don't have to have git installed, GitSharpCms uses a library that self-contains the git magic already. However, at the end of the day GitSharpCms deals with plain old git repositories, so the git executable and toolchain just works on GitSharpCms repos too, and they're an excellent toolset to investigate issues or just gain deeper understanding on what is going on in GitSharpCms repos.

#### How to clone test repos

* Change the directory to one level higher than the .sln (outside the GitSharpCms repo folder).
* Clone a developer (normal, non-bare) repository: `git clone https://gitlab.com/gerely/examplegitsharpcontentrepository.git ExampleGitSharpContentRepository`
* Clone a deployed (bare) repository: `git clone --bare https://gitlab.com/gerely/examplegitsharpcontentrepository.git ExampleGitSharpContentRepository.git`

