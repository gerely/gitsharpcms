using GitSharpCms.Repository;
using GitSharpCms.Tests.Configuration;
using GitSharpCms.Tests.SerializationTestModels;
using NUnit.Framework;

namespace GitSharpCms.Tests
{
    public class BareRepositoryTests : RepositoryTestBase
    {
        [SetUp]
        public void Setup()
        {
            var bareRepoConfig = new BareTestGitRepoConfiguration();
            _repo = GitSharpContentRepositoryFactory.Create(bareRepoConfig);
            if (!_repo.IsBare)
            {
                Assert.Fail("For these tests a bare repo needed.");
            }

            _repo.Update();
        }

        [Test]
        public void EnumerateEntries()
        {
            string selector = Selectors.PROD_BRANCH;
            string path = Paths.DASHBOARD;

            var entries = _repo.EnumerateEntries(selector, path, includeSubfolders:false);
            var entriesIncludeSubfolders = _repo.EnumerateEntries(selector, path);
        }

        [Test]
        public void GetItem()
        {
            string selector = Selectors.PROD_BRANCH;
            string path = Paths.TEST_MODEL;

            var model = _repo.GetItem<TestModel>(selector, path);
        }


    }

}