﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GitSharpCms.Tests.SerializationTestModels
{
    public class TestModel
    {
        public string StringProperty { get; set; }
        public int[] IntArraProperty { get; set; }
    }
}
