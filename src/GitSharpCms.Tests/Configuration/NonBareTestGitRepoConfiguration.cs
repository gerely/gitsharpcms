﻿using GitSharpCms.Repository;
using GitSharpCms.Repository.Interfaces;
using System.IO;

namespace GitSharpCms.Tests.Configuration
{
    class NonBareTestGitRepoConfiguration : IGitSharpContentRepositoryConfiguration
    {
        public string GitFolder
        {
            get
            {
                // If you cloned your content repositories as per the README.md, this should automagically work.
                // The ..\..\.. part will break out from .\GitSharpCms\src\GitSharpCms.Tests\bin\Debug\netcoreapp2.2
                //
                // If your repo resides elsewhere, just
                // return @"c:\path-to\non-bare-content-repo";

                var testDllPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                var testFolderfolder = Path.GetDirectoryName(testDllPath);
                return Path.Combine(testFolderfolder, @"..\..\..\..\..\..\ExampleGitSharpContentRepository");
            }
        }
        public string GitUsername => null;
        public string GitPassword => null;
    }
}