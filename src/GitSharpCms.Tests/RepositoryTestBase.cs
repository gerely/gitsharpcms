﻿using GitSharpCms.Repository.Interfaces;

namespace GitSharpCms.Tests
{
    public class RepositoryTestBase
    {
        protected IGitSharpContentRepository _repo;

        public static class Selectors
        {
            public const string PROD_BRANCH = "branch:production";
            public const string UAT_BRANCH = "branch:uat";
            public const string UAT2_BRANCH = "branch:uat2";
        }
        public static class Paths
        {
            public const string ROOT = "/";
            public const string DASHBOARD = "/dashboard";
            public const string TEST_MODEL = "/models/TestModel.json";
        }
    }
}