﻿using GitSharpCms.Repository;
using GitSharpCms.Tests.Configuration;
using NUnit.Framework;

namespace GitSharpCms.Tests
{
    public class NonBareRepositoryTests : RepositoryTestBase
    {
        [SetUp]
        public void Setup()
        {
            var configuration = new NonBareTestGitRepoConfiguration();
            _repo = GitSharpContentRepositoryFactory.Create(configuration);
            if (_repo.IsBare)
            {
                Assert.Fail("For these tests a non-bare repository needed.");
            }
        }

        [Test]
        public void EnsureNonBareRepoCannotBeUpgraded()
        {
            GitSharpContentRepositoryException e = null;
            try
            {
                _repo.Update();
            }
            catch (GitSharpContentRepositoryException ex)
            {
                e = ex;
            }

            Assert.IsTrue(e != null, "An exception must be thrown on an Update() attemp on a non-bare repo.");
        }


        [Test]
        public void Test1()
        {
        }
    }

}