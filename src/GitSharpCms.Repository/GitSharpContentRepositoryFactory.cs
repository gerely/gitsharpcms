﻿using GitSharpCms.Repository.Interfaces;

namespace GitSharpCms.Repository
{
    public static class GitSharpContentRepositoryFactory
    {
        public static IGitSharpContentRepository Create(IGitSharpContentRepositoryConfiguration configuration)
        {
            return new GitSharpContentRepository(configuration);
        }
    }
}
