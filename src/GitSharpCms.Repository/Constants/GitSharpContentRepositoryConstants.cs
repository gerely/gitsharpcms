﻿namespace GitSharpCms.Repository.Constants
{
    public static class Selectors
    {
        public const string INDEX_AREA_SELECTOR = "index";

        public const string BRANCH_SELECTOR_PREFIX = "branch:";
        public const string TAG_SELECTOR_PREFIX = "tag:";
        public const string HASH_SELECTOR_PREFIX = "hash:";

    }
}
