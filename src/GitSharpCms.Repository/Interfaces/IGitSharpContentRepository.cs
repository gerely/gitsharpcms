﻿using System.Collections.Generic;

namespace GitSharpCms.Repository.Interfaces
{
    public interface IGitSharpContentRepository
    {
        TResult GetItem<TResult>(string selector, string path) where TResult: class;
        IEnumerable<GitSharpContentRepositoryEntryDescriptor> EnumerateEntries(string selector, string path, bool includeSubfolders = true);
        void Update();
        bool IsBare { get; }
    }
}
