﻿namespace GitSharpCms.Repository.Interfaces
{
    public interface IGitSharpContentRepositoryConfiguration
    {
        string GitFolder { get; }
        string GitUsername { get; }
        string GitPassword { get; }
    }
}
