﻿namespace GitSharpCms.Repository
{
    public class GitSharpContentRepositoryEntryDescriptor
    {
        public string Path { get; internal set; }
        public bool IsFolder { get; internal set; }
    }
}
