﻿using GitSharpCms.Repository.Constants;
using GitSharpCms.Repository.Interfaces;
using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace GitSharpCms.Repository
{
    internal class GitSharpContentRepository : IGitSharpContentRepository
    {
        private readonly LibGit2Sharp.Repository _gitRepository;
        private readonly IGitSharpContentRepositoryConfiguration _repositoryConfiguration;
        private readonly UsernamePasswordCredentials _credentials;

        public bool IsBare => _gitRepository.Info.IsBare;

        public GitSharpContentRepository(IGitSharpContentRepositoryConfiguration repositoryConfiguration)
        {
            _repositoryConfiguration = repositoryConfiguration ?? throw new ArgumentNullException(nameof(repositoryConfiguration));
            if (!LibGit2Sharp.Repository.IsValid(_repositoryConfiguration.GitFolder))
            {
                throw new ArgumentException($"The {nameof(repositoryConfiguration.GitFolder)} parameter points to a non-existing or invalid git folder.");
            }

            _gitRepository = new LibGit2Sharp.Repository(_repositoryConfiguration.GitFolder);
            _credentials = new UsernamePasswordCredentials() { Username = _repositoryConfiguration.GitUsername, Password = _repositoryConfiguration.GitPassword };
        }

        public IEnumerable<GitSharpContentRepositoryEntryDescriptor> EnumerateEntries(string selector, string path, bool includeSubFolders = true)
        {
            if (selector == Selectors.INDEX_AREA_SELECTOR)
            {
                //_gitRepository.Index
                return null;
            }

            if (string.IsNullOrEmpty(path) || !path.StartsWith("/"))
            {
                throw new ArgumentException("Path must start with a slash ('/').");
            }
            path = path.TrimStart('/');

            var selectedCommit = GetCommit(selector);

            var matchingPaths = new List<TreeEntry>();
            WalkTree(selectedCommit.Tree, path, includeSubFolders, matchingPaths);

            var filePaths = matchingPaths
                .Select(te => new GitSharpContentRepositoryEntryDescriptor()
                {
                    Path = $"/{te.Path}",
                    IsFolder = te.TargetType == TreeEntryTargetType.Tree
                })
                .ToArray();
            return filePaths;
        }

        private void WalkTree(Tree tree, string path, bool recursive, List<TreeEntry> matchingPaths)
        {
            foreach (var treeEntry in tree)
            {
                if (treeEntry.Path.StartsWith(path))
                {
                    if (treeEntry.Path == path || recursive)
                    {
                        matchingPaths.Add(treeEntry);
                    }
                }

                if (treeEntry.TargetType == TreeEntryTargetType.Tree)
                {
                    WalkTree(treeEntry.Target as Tree, path, recursive, matchingPaths);
                }
            }
        }

        // param: Func<Stream, TReturn> streamToTReturnConverter ?
        public TReturn GetItem<TReturn>(string selector, string path)
            where TReturn :class
        {
            var commit = GetCommit(selector);

            if (string.IsNullOrEmpty(path) || !path.StartsWith("/"))
            {
                throw new ArgumentException("Path must starts with a slash ('/').");
            }

            path = path.TrimStart('/');

            var blob = commit[path]?.Target as Blob;

            if (blob == null)
            {
                return null;
            }

            var blobContentStream = blob.GetContentStream();

            if(typeof(TReturn) == typeof(Stream))
            {
                return blobContentStream as TReturn;
            }
            else if (typeof(TReturn) == typeof(string))
            {
                using (var reader = new StreamReader(blobContentStream))
                {
                    string str = reader.ReadToEnd();
                    return str as TReturn;
                }
            }
            else if (typeof(TReturn) == typeof(byte[]))
            {
                using (var r = new BinaryReader(blobContentStream))
                {
                    byte[] bytes = r.ReadBytes((int)blobContentStream.Length);
                    return bytes as TReturn;
                }
            }
            else
            {
                using (var reader = new StreamReader(blobContentStream))
                {
                    string str = reader.ReadToEnd();
                    var poco = Newtonsoft.Json.JsonConvert.DeserializeObject<TReturn>(str);
                    return poco;
                }
            }

        }

        public void Update()
        {
            if (!IsBare) throw new GitSharpContentRepositoryException(
                "Repository is not bare.",
                new
                {
                    RepoPath = _gitRepository.Info.Path
                }
            );
            

            if (_gitRepository.Network.Remotes.Count() != 1) throw new GitSharpContentRepositoryException(
                "There must be exactly one remote configured.",
                new
                {
                    RepoNames = _gitRepository.Network.Remotes.Select(r => r.Name)
                }
            );

            var remote = _gitRepository.Network.Remotes.Single();

            var fetchRefSpecifications = new[]
            {
                "+refs/heads/*:refs/heads/*",   // force orverride all local branches with remote branches
                "+refs/tags/*:refs/tags/*"      // the same for tags
            };

            Commands.Fetch(
                _gitRepository,
                remote.Name,
                fetchRefSpecifications,
                new FetchOptions()
                {
                    CredentialsProvider = (_url, _user, _cred) => _credentials,
                    Prune = true,
                    TagFetchMode = TagFetchMode.Auto,
                    RepositoryOperationCompleted = new LibGit2Sharp.Handlers.RepositoryOperationCompleted(OnRepositoryOperationCompleted),
                },
                "some log message" //TODO: where does it go?
            );
        }

        private void OnRepositoryOperationCompleted(RepositoryOperationContext context)
        {
            // TODO: what does it do?
            var p = context.RepositoryPath;
        }

        public Commit GetCommit(string commitSelector)
        {
            if (string.IsNullOrEmpty(commitSelector))
            {
                throw new ArgumentNullException(nameof(commitSelector));
            }

            commitSelector = commitSelector.ToLower();

            if (commitSelector.StartsWith(Selectors.BRANCH_SELECTOR_PREFIX))
            {
                var branchSelector = commitSelector.Substring(Selectors.BRANCH_SELECTOR_PREFIX.Length);
                return GetCommitByBranch(branchSelector);
            }
            else if (commitSelector.StartsWith(Selectors.TAG_SELECTOR_PREFIX))
            {
                var tagSelector = commitSelector.Substring(Selectors.TAG_SELECTOR_PREFIX.Length);
                return GetCommitByTag(tagSelector);
            }
            else if (commitSelector.StartsWith(Selectors.HASH_SELECTOR_PREFIX))
            {
                var hashSelector = commitSelector.Substring(Selectors.TAG_SELECTOR_PREFIX.Length);
                return GetCommitByHash(hashSelector);
            }
            else
            {
                throw new ArgumentException("Invalid commit selector.");
            }
        }

        #region GetCommitBy...() private methods ----------------------------------------------------------------------------------

        private Commit GetCommitByBranch(string lowercaseBranchName)
        {
            var selectedBranch = _gitRepository
                .Branches
                .FirstOrDefault(b => b.FriendlyName?.ToLower() == lowercaseBranchName);
            var lastCommitOnBranch = selectedBranch?.Commits?.FirstOrDefault();
            return lastCommitOnBranch;
        }

        private Commit GetCommitByHash(string hashSelector)
        {
            var commit = _gitRepository
                    .Commits
                    .FirstOrDefault(c => c.Sha?.ToLower() == hashSelector);
            return commit;
        }

        private Commit GetCommitByTag(string tagSelector)
        {
            var selectedTag = _gitRepository
                .Tags
                .FirstOrDefault(t => t.FriendlyName?.ToLower() == tagSelector);
            return selectedTag?.Target as Commit;
        }
        #endregion

    }
}
