﻿using System;

namespace GitSharpCms.Repository
{
    public class GitSharpContentRepositoryException : ApplicationException
    {
        public object CustomData { get; }
        public GitSharpContentRepositoryException(string message, object customData = null)
            : base(message)
        {
            CustomData = customData;
        }
    }
}
