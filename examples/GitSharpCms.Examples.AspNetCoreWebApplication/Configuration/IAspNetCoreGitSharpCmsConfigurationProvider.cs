﻿using GitSharpCms.Repository.Interfaces;

namespace GitSharpCms.Examples.AspNetCoreWebApplication.Configuration
{
    public interface IAspNetCoreGitSharpCmsConfigurationProvider
        : IGitSharpContentRepositoryConfiguration
    {
        string DefaultSelector { get; }
    }
}
