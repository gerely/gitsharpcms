﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GitSharpCms.Examples.AspNetCoreWebApplication.Configuration
{
    public class AspNetCoreGitSharpCmsConfigurationProvider
        : IAspNetCoreGitSharpCmsConfigurationProvider
    {
        private readonly IConfiguration _configuration;
        private readonly Microsoft.Extensions.Hosting.IHostingEnvironment _hostingEnvironment;

        public string DefaultSelector => _configuration[$"GitSharpCms:{nameof(DefaultSelector)}"];
        public string GitUsername => _configuration[$"GitSharpCms:{nameof(GitUsername)}"];
        public string GitPassword => _configuration[$"GitSharpCms:{nameof(GitPassword)}"];
        public string GitFolder
        {
            get
            {
                var configGitFolder = _configuration[$"GitSharpCms:{nameof(GitFolder)}"];

                // C:\GSCms\GitSharpCms\examples\GitSharpCms.Examples.AspNetCoreWebApplication
                var contentRootPath = _hostingEnvironment.ContentRootPath;
                return configGitFolder.Replace("~", contentRootPath);
            }
        }

        public AspNetCoreGitSharpCmsConfigurationProvider(IConfiguration configuration, Microsoft.Extensions.Hosting.IHostingEnvironment hostingEnvironment)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _hostingEnvironment = hostingEnvironment ?? throw new ArgumentNullException(nameof(hostingEnvironment));
        }
    }
}
