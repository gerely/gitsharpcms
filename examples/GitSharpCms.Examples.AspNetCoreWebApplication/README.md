﻿There's a nuget.config in next to the .sln file.
That adds the LocalNugetSource entry, so the build artifact nuget package from GitSharpCms.sln can be added to this project like any other nuget package. Go to Manage NuGet Packages, and make sure the Package Source in the upper right corner set correctly.

AspNetCoreGitSharpCmsConfigurationProvider added.