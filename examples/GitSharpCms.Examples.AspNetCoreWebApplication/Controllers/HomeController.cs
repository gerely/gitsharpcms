﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GitSharpCms.Examples.AspNetCoreWebApplication.Models;
using GitSharpCms.Repository.Interfaces;
using GitSharpCms.Examples.AspNetCoreWebApplication.Configuration;

namespace GitSharpCms.Examples.AspNetCoreWebApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly IGitSharpContentRepository _repository;
        private readonly IAspNetCoreGitSharpCmsConfigurationProvider _repositoryConfiguration;

        public HomeController(IGitSharpContentRepository repository, IAspNetCoreGitSharpCmsConfigurationProvider repositoryConfiguration)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _repositoryConfiguration = repositoryConfiguration ?? throw new ArgumentNullException(nameof(repositoryConfiguration));
        }

        public IActionResult Index(string selectorOverride)
        {
            if (_repository.IsBare)
            {
                _repository.Update();
            }

            string selector = selectorOverride ?? _repositoryConfiguration.DefaultSelector;

            var dashboardItemEntries = _repository.EnumerateEntries(selector, "/dashboard/", includeSubfolders:true);
            var dashboardItems = new List<string>();
            foreach (var entry in dashboardItemEntries.Where(e => !e.IsFolder && e.Path.EndsWith(".html")))
            {
                var dashboardItem = _repository.GetItem<string>(selector, entry.Path);
                dashboardItems.Add(dashboardItem);
            }
            
            
            return View(dashboardItems);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
